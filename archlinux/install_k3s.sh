paru -Sy --needed k3s-1.22-bin kubectl \
  && sudo systemctl enable k3s \
  && sudo systemctl start k3s \
  && mkdir -p "${HOME}/.kube" \
  && sudo cp /etc/rancher/k3s/k3s.yaml "${HOME}/.kube/config" \
  && sudo chown $(id -u):$(id -g) "${HOME}/.kube/config"


kubectl -n kube-system get pods
