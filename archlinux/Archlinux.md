## VirtualEnv

```bash
sudo pacman -S  python-virtualenv
virtualenv venv
source venv/bin/activate
```

## Gedit

```bash
pacman -S extra/geydit extra/gedit-plugins
```


## Pip (Python)

```bash
sudo pacman -S extra/python-pip
```

# Docker
```bash
 sudo pacman -S docker
 systemctl status docker.service
  docker
  sudo systemctl enable docker.service
  sudo systemctl start docker.service
  sudo usermod -aG docker $USER
  docker run hello-world
```

## ligthDM

[greeter]
theme-name=TraditionalOk
icon-theme-name=mate-black
background=/usr/share/pixmaps/Arch-Linux-Wallpaper-Full-HD.png
#background=/usr/share/backgrounds/mate/nature/Garden.jpg
default-user-image=/home/$USER/.cb500x.jpg
