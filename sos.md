## Compress tar.gz
```bash
tar -czvf archive.tar.gz stuff
```
## Extract
```bash
tar -xzvf archive.tar.gz
```


# Networking
* Interfaces:
```bash
ifconfig eth0 down
ifconfig eth0 up
/etc/init.d/network stop
service network stop  
/etc/init.d/network start
service network start
```
