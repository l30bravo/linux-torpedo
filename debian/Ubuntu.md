# Ubuntu

## base
```bash
apt install gnome-tweaks gnome-tweak-tool
apt install appmenu-gtk appmenu-gtk3
apt install python3-pip
```

## VPN
```bash
apt install network-manager-openconnect
apt install network-manager-openconnect-gnome
```
