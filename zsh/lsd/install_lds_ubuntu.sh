snap install lsd
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/Hack.zip 
mkdir ~/.fonts
mv Hack.zip ~/.fonts
cd ~/fonts
unzip Hack.zip 
rm Hack.zip
fc-cache -fv
