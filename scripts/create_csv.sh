COL_RUTS=""
COL_ID_TUI=""
#COL_ESTADO=""
SEPARADOR=","
NEW_CSV="new_csv.csv"

CONT=0
while read LINE;
do 
	if [ $CONT -lt 20 ]; then
		RUT=$(echo $LINE | awk -F',' {'print $2'})
		ID_TUI=$(echo $LINE | awk -F',' {'print $1'})
		if [ $CONT -eq 0  ];then
			SEPARADOR=""
		else
			SEPARADOR=","
		fi
		COL_RUTS="${COL_RUTS}${SEPARADOR}${RUT}"
		COL_ID_TUI="${COL_ID_TUI}${SEPARADOR}${ID_TUI}"
		echo $CONT"-"$LINE"-"$RUT"-"$ID_TUI
		((CONT = $CONT +1))
	else
		echo "${COL_RUTS},${COL_ID_TUI}" >>$NEW_CSV
		CONT=0
		COL_RUTS=""
		COL_ID_TUI=""
	fi
done <mini.csv

echo "${COL_RUTS},${COL_ID_TUI}">>$NEW_CSV

