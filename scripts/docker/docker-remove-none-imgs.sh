docker images | grep none | grep -v openfaas | grep -v alpine | grep -v prom | grep -v nats | awk {'print $3'} > list-img.txt

while read LINE; do
	echo "remove "$LINE
	docker rmi $LINE
done < list-img.txt

docker images
